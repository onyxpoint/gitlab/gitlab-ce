# frozen_string_literal: true

require 'omniauth'

module OmniAuth
  module Strategies
    class OAuth2
      include OmniAuth::Strategy
      credentials do
        hash = {"token" => access_token.token}
        hash["refresh_token"] = access_token.refresh_token if access_token.expires? && access_token.refresh_token
        hash["expires_at"] = access_token.expires_at if access_token.expires?
        hash["expires"] = access_token.expires?
        hash["IAL"] = access_token.params["IAL"]
        hash["AAL"] = access_token.params["AAL"]
        hash
      end
    end
  end
end
