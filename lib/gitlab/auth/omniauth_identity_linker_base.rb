# frozen_string_literal: true

module Gitlab
  module Auth
    class OmniauthIdentityLinkerBase
      attr_reader :current_user, :oauth

      def initialize(current_user, oauth)
        @current_user = current_user
        @oauth = oauth
        @changed = false
      end

      def link
        save if unlinked?
      end

      def update(oauth)
        @oauth = oauth
        identity.save
      end
      
      def changed?
        @changed
      end
      
      def updatable?
        @identity ||= @current_user.identities.with_extern_uid(provider, uid).first
        @identity.expires
      end

      def failed?
        error_message.present?
      end

      def error_message
        identity.validate

        identity.errors.full_messages.join(', ')
      end

      private

      def save
        @changed = identity.save
      end

      def unlinked?
        identity.new_record?
      end

      # rubocop: disable CodeReuse/ActiveRecord
      def identity
        @identity ||= current_user.identities
                                  .with_extern_uid(provider, uid)
                                  .first_or_initialize(extern_uid: uid)
        @identity.token = token
        @identity.expires = expires
        @identity.expires_at = expires_at
        @identity.ial = ial
        @identity.aal = aal

        @identity
      end
      # rubocop: enable CodeReuse/ActiveRecord

      def provider
        @oauth['provider']
      end

      def uid
        @oauth['uid']
      end

      def token
        @oauth.dig(:credentials, :token)
      end

      def expires
        @oauth.dig(:credentials, :expires)
      end

      def expires_at
        @oauth.dig(:credentials, :expires_at)
      end

      def ial
        @oauth.dig(:credentials, :IAL)
      end

      def aal
        @oauth.dig(:credentials, :AAL)
      end
    end
  end
end
